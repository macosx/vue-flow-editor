module.exports = {
    sourceType: 'unambiguous',          // 打包G6的时候才需要的属性
    presets: ['vca-jsx', '@vue/app'],
    plugins: ["@babel/plugin-transform-modules-umd"]
};
